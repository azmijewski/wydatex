create table user_entity
(
    id         uuid PRIMARY KEY,
    email      varchar(255) unique,
    password   varchar(255),
    firstname  varchar(50)  not null,
    lastname   varchar(100) not null,
    role       varchar(15)  not null,
    version    int          not null default 0,
    enabled    bool                  default false,
    created_at timestamp,
    deleted_at timestamp
)