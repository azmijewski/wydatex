create table come_entity
(
    id          uuid PRIMARY KEY,
    price       DECIMAL   not null default 0,
    date        TIMESTAMP not null,
    description text,
    user_id     uuid,

    constraint user_come_id_fk FOREIGN KEY (user_id) references user_entity (id)
)