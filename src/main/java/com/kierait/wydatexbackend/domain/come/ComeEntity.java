package com.kierait.wydatexbackend.domain.come;

import com.kierait.wydatexbackend.domain.user.UserEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "come_type")
@Getter
public abstract class ComeEntity {
    @Id
    private UUID id;
    private BigDecimal price;
    private LocalDate date;
    private String description;
    @Column(name = "deleted_at")
    private LocalDate deletedAt;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    public ComeEntity(BigDecimal price, LocalDate date, String description, UserEntity user) {
        this.id = UUID.randomUUID();
        this.price = price;
        this.date = date;
        this.description = description;
        this.user = user;
    }

    public void update(BigDecimal price, LocalDate date, String description) {
        this.price = price;
        this.date = date;
        this.description = description;
    }

    public void anonymize() {
        price = BigDecimal.ZERO;
        description = "deleted";
        deletedAt = LocalDate.now();
    }
}
