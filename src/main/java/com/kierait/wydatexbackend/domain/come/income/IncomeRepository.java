package com.kierait.wydatexbackend.domain.come.income;

import com.kierait.wydatexbackend.domain.user.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.UUID;

@Repository
public interface IncomeRepository extends JpaRepository<IncomeEntity, UUID> {
    @Query("select i from IncomeEntity i " +
            "where i.user.id = :userId " +
            "and i.date >= :startDate " +
            "and i.date <= :endDate")
    Collection<IncomeEntity> findAllByUserIdAndDateBetween(@Param("userId") UUID id,
                                             @Param("startDate") LocalDate startDate,
                                             @Param("endDate") LocalDate endDate);

    Collection<IncomeEntity> findAllByUser(UserEntity userEntity);
}
