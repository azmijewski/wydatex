package com.kierait.wydatexbackend.domain.come.income;

import com.kierait.wydatexbackend.domain.come.ComeEntity;
import com.kierait.wydatexbackend.domain.user.UserEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@DiscriminatorValue("INCOME")
public class IncomeEntity extends ComeEntity {

    public IncomeEntity(BigDecimal price, LocalDate date, String description, UserEntity userEntity) {
        super(price, date, description, userEntity);
    }
}
