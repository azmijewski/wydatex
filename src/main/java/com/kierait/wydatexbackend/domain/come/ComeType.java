package com.kierait.wydatexbackend.domain.come;

public enum ComeType {
    INCOME, OUTCOME
}
