package com.kierait.wydatexbackend.domain.come;

import com.kierait.wydatexbackend.application.come.model.ComeDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ComeRepository extends JpaRepository<ComeEntity, UUID> {
    @Query("select c from ComeEntity c " +
            "where c.id = :id " +
            "and c.user.id = :userId")
    Optional<ComeEntity> findByIdAndUserId(@Param("id") UUID id,
                                           @Param("userId") UUID userId);

    @Query("select c from ComeEntity c " +
            "where c.user.id = :id")
    List<ComeEntity> findByUserId(@Param("id") UUID userId, Pageable pageable);

    @Query("select c from ComeEntity c " +
            "where c.user.id = :id " +
            "and c.date > :date")
    Collection<ComeEntity> findAllByUserAfterDate(@Param("id") UUID userId,
                                                  @Param("date") LocalDate date);

    Collection<ComeEntity> findAllByUser_Id(UUID userId);
}
