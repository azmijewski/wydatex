package com.kierait.wydatexbackend.domain.come.outcome;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.UUID;

@Repository
public interface OutcomeRepository extends JpaRepository<OutcomeEntity, UUID> {
    @Query("select o from OutcomeEntity o " +
            "where o.user.id = :userId " +
            "and o.date > :startDate " +
            "and o.date < :endDate")
    Collection<OutcomeEntity> findAllByUserIdAndDateBetween(@Param("userId") UUID id,
                                                           @Param("startDate") LocalDate startDate,
                                                           @Param("endDate") LocalDate endDate);
}
