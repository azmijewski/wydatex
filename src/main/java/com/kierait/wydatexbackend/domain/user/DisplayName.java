package com.kierait.wydatexbackend.domain.user;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@EqualsAndHashCode
@Getter
@Embeddable
public class DisplayName implements Serializable {
    private String firstname;
    private String lastname;
}
