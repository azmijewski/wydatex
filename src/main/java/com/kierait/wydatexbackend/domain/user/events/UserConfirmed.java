package com.kierait.wydatexbackend.domain.user.events;

import lombok.Value;

import java.util.UUID;

@Value
public class UserConfirmed {
    UUID userId;
}
