package com.kierait.wydatexbackend.domain.user;

import com.kierait.wydatexbackend.domain.user.events.UserConfirmed;
import com.kierait.wydatexbackend.domain.user.events.UserCreated;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
public class UserEntity extends AbstractAggregateRoot<UserEntity> {
    private static final String DELETED = "deleted_user";
    private static final String DELETED_EMAIl = "deleted@deleted.com";

    @Id
    @Getter
    private UUID id;

    @Email
    @Getter
    private String email;

    @Getter
    private String password;

    @Embedded
    @Getter
    private DisplayName displayName;

    @Version
    private int version;

    @Enumerated(EnumType.STRING)
    @Getter
    private Role role;

    @Getter
    private boolean enabled;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "confirmation_code")
    @Getter
    private UUID confirmationCode;

    public UserEntity(@Email String email, String password, DisplayName displayName) {
        this.id = UUID.randomUUID();
        this.version = 0;
        this.email = email;
        this.password = password;
        this.displayName = displayName;
        this.role = Role.ROLE_USER;
        this.enabled = false;
        this.createdAt = LocalDateTime.now();
        this.confirmationCode = UUID.randomUUID();
        registerEvent(new UserCreated(this.id));
    }

    public void enable() {
        this.enabled = true;
        this.confirmationCode = null;
        registerEvent(new UserConfirmed(this.id));
    }

    public void delete() {
        this.email = DELETED_EMAIl;
        this.displayName = new DisplayName(DELETED, DELETED);
        this.enabled = false;
        this.deletedAt = LocalDateTime.now();
        this.password = null;
    }

    public void createCode() {
        this.confirmationCode = UUID.randomUUID();
        registerEvent(new UserCreated(id));
    }
}
