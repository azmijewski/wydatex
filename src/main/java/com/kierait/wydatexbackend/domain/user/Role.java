package com.kierait.wydatexbackend.domain.user;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
