package com.kierait.wydatexbackend.api;

import com.kierait.wydatexbackend.application.come.ComeNotFound;
import com.kierait.wydatexbackend.application.come.ComeService;
import com.kierait.wydatexbackend.application.come.IncomeService;
import com.kierait.wydatexbackend.application.come.OutcomeService;
import com.kierait.wydatexbackend.application.come.model.ComeDTO;
import com.kierait.wydatexbackend.application.come.model.MonthComeDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("api/comes")
@RequiredArgsConstructor
public class ComeController {
    private final ComeService comeService;
    private final IncomeService incomeService;
    private final OutcomeService outcomeService;

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void on(ComeNotFound ex){}

    @PostMapping("/incomes")
    @ResponseStatus(HttpStatus.CREATED)
    public void addIncome(@RequestBody @Valid ComeDTO comeDTO) {
        incomeService.addIncome(comeDTO);
    }

    @GetMapping("/incomes")
    public Collection<ComeDTO> getIncomesBetweenDate(@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate startDate,
                                                     @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate endDate) {
        return incomeService.getAllByUserBetweenDate(startDate, endDate);
    }

    @PostMapping("/outcomes")
    @ResponseStatus(HttpStatus.CREATED)
    public void addOutcome(@RequestBody @Valid ComeDTO comeDTO) {
        outcomeService.addOutcome(comeDTO);
    }

    @GetMapping("/outcomes")
    public Collection<ComeDTO> getOutcomesBetweenDate(@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate startDate,
                                                     @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate endDate) {
        return outcomeService.getAllByUserBetweenDate(startDate, endDate);
    }

    @DeleteMapping("/{id}")
    public void deleteCome(@PathVariable(name = "id") UUID id) {
        comeService.deleteCome(id);
    }

    @GetMapping
    public Collection<ComeDTO> getLastComes() {
        return comeService.getLastComes();
    }

    @PutMapping("/{id}")
    public void updateCome(@PathVariable(name = "id") UUID id,
                           @RequestBody @Valid ComeDTO comeDTO) {
        comeService.updateCome(id, comeDTO);
    }

    @GetMapping("/byMonth")
    public Collection<MonthComeDTO> getByMonths() {
        return comeService.getMonthComes();
    }

}
