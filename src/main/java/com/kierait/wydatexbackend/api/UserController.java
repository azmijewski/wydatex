package com.kierait.wydatexbackend.api;

import com.kierait.wydatexbackend.application.user.ConfirmationCodeNotFound;
import com.kierait.wydatexbackend.application.user.UserAlreadyExist;
import com.kierait.wydatexbackend.application.user.UserService;
import com.kierait.wydatexbackend.application.user.model.RegisterUser;
import com.kierait.wydatexbackend.application.user.model.ResendCode;
import com.kierait.wydatexbackend.application.user.model.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    public void on(UserAlreadyExist ex) {}

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void on(ConfirmationCodeNotFound ex) {}

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerUser(@RequestBody @Valid RegisterUser registerUser) {
        userService.registerUser(registerUser);
    }

    @GetMapping("/login")
    public UserDTO login() {
        return userService.getLoggedUser();
    }

    @PutMapping("/confirmation/{code}")
    public void enableUser(@PathVariable(name = "code") String code) {
        System.out.println("Inside method");
        userService.enableUser(UUID.fromString(code));
    }

    @PutMapping("/verification-code")
    public void resendCode(@RequestBody @Valid ResendCode resendCode) {
        userService.resendVerificationCode(resendCode);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        userService.deleteUser(id);
    }
}
