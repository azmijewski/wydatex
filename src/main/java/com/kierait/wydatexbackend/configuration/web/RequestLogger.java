package com.kierait.wydatexbackend.configuration.web;

import com.kierait.wydatexbackend.application.AuthenticatedService;
import com.kierait.wydatexbackend.configuration.security.UserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class RequestLogger extends OncePerRequestFilter {
    private static final String X_FORWARDED_FOR = "X-Forwarded-For";

    private final AuthenticatedService authenticatedService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        long startTimestamp = System.currentTimeMillis();
        try {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } finally {
            long endTimestamp = System.currentTimeMillis();
            log.info("Request url: {}, method: {}, ip: {}, userId: {}, result: {}, executionTime: {}",
                    httpServletRequest.getRequestURI(), httpServletRequest.getMethod(),
                    getIp(httpServletRequest), getUserId(), httpServletResponse.getStatus(),
                    endTimestamp - startTimestamp);
        }
    }

    private String getIp(HttpServletRequest httpServletRequest) {
        return Optional.ofNullable(httpServletRequest.getHeader(X_FORWARDED_FOR))
                .orElse(httpServletRequest.getRemoteAddr());
    }

    private UUID getUserId() {
        return Optional.ofNullable(authenticatedService.getUserInfo())
                .map(UserInfo::getId)
                .orElse(null);
    }
}
