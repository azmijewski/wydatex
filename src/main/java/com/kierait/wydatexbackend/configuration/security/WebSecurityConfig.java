package com.kierait.wydatexbackend.configuration.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String[] AUTHENTICATED_PATHS= {
            "/api/comes/**",
            "/api/users/login"
    };
    private static final String[] ADMIN_PATHS = {
            "/api/admin/**"
    };
    private static final String[] PERMIT_ALL_PATHS = {
            "/api/users/registration",
            "/api/users/confirmation/*"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers(ADMIN_PATHS).hasRole("ADMIN")
                    .antMatchers(AUTHENTICATED_PATHS).authenticated()
                    .antMatchers(PERMIT_ALL_PATHS).permitAll()
                .and()
                .cors()
                .and()
                .csrf().disable()
                .httpBasic()
                .and()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
