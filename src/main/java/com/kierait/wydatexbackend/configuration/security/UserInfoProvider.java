package com.kierait.wydatexbackend.configuration.security;

import com.kierait.wydatexbackend.domain.user.UserEntity;
import com.kierait.wydatexbackend.domain.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UserInfoProvider implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username)
                .filter(UserEntity::isEnabled)
                .map(this::map)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    private UserInfo map(UserEntity userEntity) {
        return new UserInfo(userEntity.getId(), userEntity.getEmail(), userEntity.getPassword(),
                List.of(new SimpleGrantedAuthority(userEntity.getRole().name())));
    }
}
