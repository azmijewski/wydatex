package com.kierait.wydatexbackend.application.user;

import java.util.UUID;

public class ConfirmationCodeNotFound extends RuntimeException {
    public ConfirmationCodeNotFound(UUID code) {
        super(String.format("Could not found code %s", code));
    }
}
