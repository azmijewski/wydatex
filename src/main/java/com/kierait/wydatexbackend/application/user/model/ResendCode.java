package com.kierait.wydatexbackend.application.user.model;

import lombok.Value;

import javax.validation.constraints.Email;
import java.io.Serializable;

@Value
public class ResendCode implements Serializable {
    @Email
    String email;
}
