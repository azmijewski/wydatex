package com.kierait.wydatexbackend.application.user;

public class UserAlreadyExist extends RuntimeException {

    public UserAlreadyExist(String email) {
        super(String.format("User: %s already exist", email));
    }
}
