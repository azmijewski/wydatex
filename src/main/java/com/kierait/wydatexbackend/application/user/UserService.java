package com.kierait.wydatexbackend.application.user;

import com.kierait.wydatexbackend.application.AuthenticatedService;
import com.kierait.wydatexbackend.application.come.ComeAnonymizer;
import com.kierait.wydatexbackend.application.user.model.ResendCode;
import com.kierait.wydatexbackend.application.user.model.UserDTO;
import com.kierait.wydatexbackend.domain.user.UserRepository;
import com.kierait.wydatexbackend.application.user.model.RegisterUser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserFactory userFactory;
    private final UserMapper userMapper;
    private final AuthenticatedService authenticatedService;
    private final ComeAnonymizer comeAnonymizer;

    @Transactional
    public void registerUser(RegisterUser registerUser) {
        if (userRepository.existsByEmail(registerUser.getEmail())) {
            registerUser.utilizePassword();
            throw new UserAlreadyExist(registerUser.getEmail());
        }
        var user = userFactory.createUser(registerUser);
        userRepository.save(user);
    }

    @Transactional
    public UserDTO getLoggedUser() {
        var user = userRepository.getOne(authenticatedService.getUserId());
        return userMapper.mapToDTO(user);
    }

    @Transactional
    public void enableUser(UUID code) {
       var user = userRepository.findByConfirmationCode(code)
               .orElseThrow(() -> new ConfirmationCodeNotFound(code));
       user.enable();
       userRepository.save(user);
    }

    @Transactional
    public void resendVerificationCode(ResendCode resendCode) {
        userRepository.findByEmailAndEnabledFalse(resendCode.getEmail())
                .ifPresent(user -> {
                    user.createCode();
                    userRepository.save(user);
                });
    }

    @Transactional
    public void deleteUser(UUID userId) {
        var user = userRepository.getOne(userId);
        user.delete();
        comeAnonymizer.anonymizeForUser(userId);
        userRepository.save(user);
    }
}
