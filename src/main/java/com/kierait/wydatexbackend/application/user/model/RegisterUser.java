package com.kierait.wydatexbackend.application.user.model;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Arrays;

@Value
public class RegisterUser implements Serializable {
    @NotEmpty
    @Email
    String email;
    @NotNull
    char[] password;
    @NotEmpty
    String firstName;
    @NotEmpty
    String lastName;

    public void utilizePassword() {
        Arrays.fill(password, '*');
    }
}
