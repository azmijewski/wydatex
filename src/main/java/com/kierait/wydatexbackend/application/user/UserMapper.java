package com.kierait.wydatexbackend.application.user;

import com.kierait.wydatexbackend.application.user.model.UserDTO;
import com.kierait.wydatexbackend.domain.user.UserEntity;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {
    UserDTO mapToDTO(UserEntity userEntity);
}
