package com.kierait.wydatexbackend.application.user;

import com.kierait.wydatexbackend.domain.user.DisplayName;
import com.kierait.wydatexbackend.domain.user.UserEntity;
import com.kierait.wydatexbackend.application.user.model.RegisterUser;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserFactory {
    private final PasswordEncoder passwordEncoder;

    public UserEntity createUser(RegisterUser registerUser) {
        var password = passwordEncoder.encode(new String(registerUser.getPassword()));
        registerUser.utilizePassword();
        var displayName = new DisplayName(registerUser.getFirstName(), registerUser.getLastName());
        return new UserEntity(registerUser.getEmail(), password, displayName);
    }
}
