package com.kierait.wydatexbackend.application.user.model;

import com.kierait.wydatexbackend.domain.user.DisplayName;
import com.kierait.wydatexbackend.domain.user.Role;
import lombok.Value;

import java.io.Serializable;
import java.util.UUID;

@Value
public class UserDTO implements Serializable {
    UUID id;
    String email;
    DisplayName displayName;
    Role role;
}
