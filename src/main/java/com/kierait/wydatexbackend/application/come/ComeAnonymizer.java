package com.kierait.wydatexbackend.application.come;

import com.kierait.wydatexbackend.domain.come.ComeEntity;
import com.kierait.wydatexbackend.domain.come.ComeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ComeAnonymizer {
    private final ComeRepository comeRepository;

    @Transactional
    public void anonymizeForUser(UUID userId) {
        var userComes = comeRepository.findAllByUser_Id(userId);
        userComes.forEach(ComeEntity::anonymize);
        comeRepository.saveAll(userComes);
    }
}
