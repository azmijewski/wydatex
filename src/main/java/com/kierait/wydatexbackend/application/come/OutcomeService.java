package com.kierait.wydatexbackend.application.come;

import com.kierait.wydatexbackend.application.AuthenticatedService;
import com.kierait.wydatexbackend.application.come.model.ComeDTO;
import com.kierait.wydatexbackend.domain.come.outcome.OutcomeEntity;
import com.kierait.wydatexbackend.domain.come.outcome.OutcomeRepository;
import com.kierait.wydatexbackend.domain.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OutcomeService {
    private final OutcomeRepository outcomeRepository;
    private final UserRepository userRepository;
    private final ComeMapper comeMapper;
    private final AuthenticatedService authenticatedService;

    @Transactional
    public void addOutcome(ComeDTO comeDTO) {
        var user = userRepository.getOne(authenticatedService.getUserId());
        var outcome = new OutcomeEntity(comeDTO.getPrice(), comeDTO.getDate(), comeDTO.getDescription(), user);
        outcomeRepository.save(outcome);
    }

    @Transactional(readOnly = true)
    public Collection<ComeDTO> getAllByUserBetweenDate(LocalDate startDate, LocalDate endDate) {
        return outcomeRepository.findAllByUserIdAndDateBetween(authenticatedService.getUserId(), startDate, endDate)
                .stream()
                .map(comeMapper::mapToDTO)
                .collect(Collectors.toUnmodifiableList());
    }
}
