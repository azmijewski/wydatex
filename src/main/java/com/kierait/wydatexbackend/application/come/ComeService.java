package com.kierait.wydatexbackend.application.come;

import com.kierait.wydatexbackend.application.AuthenticatedService;
import com.kierait.wydatexbackend.application.come.model.ComeDTO;
import com.kierait.wydatexbackend.application.come.model.MonthComeDTO;
import com.kierait.wydatexbackend.domain.come.ComeRepository;
import com.kierait.wydatexbackend.domain.come.ComeType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class ComeService {
    private static final int LAST_COMES_NUMBER = 15;

    private final ComeRepository comeRepository;
    private final ComeMapper comeMapper;
    private final AuthenticatedService authenticatedService;

    @Transactional
    public void deleteCome(UUID id) {
        comeRepository.findByIdAndUserId(id, authenticatedService.getUserId())
                .ifPresentOrElse(comeRepository::delete,
                        () -> { throw new ComeNotFound(id, authenticatedService.getUserId());});
    }

    @Transactional(readOnly = true)
    public Collection<ComeDTO> getLastComes() {
        var pageable = PageRequest.of(0, LAST_COMES_NUMBER, Sort.by("date").descending());
        return comeRepository.findByUserId(authenticatedService.getUserId(), pageable)
                .stream()
                .map(comeMapper::mapToDTO)
                .collect(Collectors.toUnmodifiableList());
    }

    @Transactional
    public void updateCome(UUID id, ComeDTO comeDTO) {
        var come = comeRepository.findByIdAndUserId(id, authenticatedService.getUserId())
                .orElseThrow(() -> new ComeNotFound(id, authenticatedService.getUserId()));
        come.update(comeDTO.getPrice(), comeDTO.getDate(), comeDTO.getDescription());
        comeRepository.save(come);
    }

    @Transactional(readOnly = true)
    public Collection<MonthComeDTO> getMonthComes() {
        var now = LocalDate.now();
        var fromDate = now.withDayOfMonth(1).minusYears(2);
        var comes = comeRepository.findAllByUserAfterDate(authenticatedService.getUserId(), fromDate)
                .stream()
                .map(comeMapper::mapToDTO)
                .collect(Collectors.toUnmodifiableList());

        var months = IntStream.range(0, 25)
                .mapToObj(fromDate::plusMonths)
                .collect(Collectors.toUnmodifiableList());

        var byMonths = months.stream()
                .collect(Collectors.toMap((date -> date),
                        date ->  new ArrayList<ComeDTO>()));

        comes.forEach(comeDTO -> byMonths.get(comeDTO.getDate().withDayOfMonth(1)).add(comeDTO));

        return byMonths.keySet()
                .stream()
                .filter(date -> !byMonths.get(date).isEmpty())
                .sorted(Comparator.reverseOrder())
                .map(date -> mapToMonthComeDTO(now, date, byMonths.get(date)))
                .collect(Collectors.toUnmodifiableList());
    }

    private MonthComeDTO mapToMonthComeDTO(LocalDate now, LocalDate date, ArrayList<ComeDTO> comes) {
        var sum = comes.stream()
                .map(come -> {
                    if (come.getComeType() == ComeType.INCOME) {
                        return come.getPrice();
                    }
                    return come.getPrice().negate();
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return new MonthComeDTO(date.getMonthValue(), date.getYear(), sum, comes, now.withDayOfMonth(1).equals(date));
    }

}
