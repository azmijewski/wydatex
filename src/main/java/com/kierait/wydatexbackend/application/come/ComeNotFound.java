package com.kierait.wydatexbackend.application.come;

import java.util.UUID;

public class ComeNotFound extends RuntimeException {
    public ComeNotFound(UUID comeId, UUID userId) {
        super(String.format("Could not found come with id: %s for user: %s", comeId, userId));
    }
}
