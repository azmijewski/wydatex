package com.kierait.wydatexbackend.application.come;

import com.kierait.wydatexbackend.application.AuthenticatedService;
import com.kierait.wydatexbackend.application.come.model.ComeDTO;
import com.kierait.wydatexbackend.domain.come.income.IncomeEntity;
import com.kierait.wydatexbackend.domain.come.income.IncomeRepository;
import com.kierait.wydatexbackend.domain.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class IncomeService {
    private final IncomeRepository incomeRepository;
    private final UserRepository userRepository;
    private final ComeMapper comeMapper;
    private final AuthenticatedService authenticatedService;

    @Transactional
    public void addIncome(ComeDTO comeDTO) {
        var user = userRepository.getOne(authenticatedService.getUserId());
        var income = new IncomeEntity(comeDTO.getPrice(), comeDTO.getDate(), comeDTO.getDescription(), user);
        incomeRepository.save(income);
    }

    @Transactional(readOnly = true)
    public Collection<ComeDTO> getAllByUserBetweenDate(LocalDate startDate, LocalDate endDate) {
        return incomeRepository.findAllByUserIdAndDateBetween(authenticatedService.getUserId(), startDate, endDate)
                .stream()
                .map(comeMapper::mapToDTO)
                .collect(Collectors.toUnmodifiableList());
    }
}
