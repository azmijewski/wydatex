package com.kierait.wydatexbackend.application.come;

import com.kierait.wydatexbackend.application.come.model.ComeDTO;
import com.kierait.wydatexbackend.domain.come.ComeEntity;
import com.kierait.wydatexbackend.domain.come.income.IncomeEntity;
import com.kierait.wydatexbackend.domain.come.outcome.OutcomeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ComeMapper {
    @Mapping(target = "comeType", constant = "INCOME")
    ComeDTO mapToDTO(IncomeEntity incomeEntity);
    @Mapping(target = "comeType", constant = "OUTCOME")
    ComeDTO mapToDTO(OutcomeEntity outcomeEntity);
    @Mapping(target = "comeType", expression = "java(comeEntity instanceof IncomeEntity ? ComeType.INCOME : ComeType.OUTCOME)")
    ComeDTO mapToDTO(ComeEntity comeEntity);
}
