package com.kierait.wydatexbackend.application.come.model;

import lombok.Value;

import java.math.BigDecimal;
import java.util.Collection;

@Value
public class MonthComeDTO {
    int month;
    int year;
    BigDecimal sum;
    Collection<ComeDTO> comes;
    boolean current;
}
