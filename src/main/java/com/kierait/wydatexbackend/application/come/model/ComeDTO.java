package com.kierait.wydatexbackend.application.come.model;

import com.kierait.wydatexbackend.domain.come.ComeType;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Value
public class ComeDTO {
    @Null
    UUID id;
    @NotNull
    BigDecimal price;
    @NotNull
    LocalDate date;
    String description;
    @Null
    ComeType comeType;
}
