package com.kierait.wydatexbackend.application;

import com.kierait.wydatexbackend.configuration.security.UserInfo;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class AuthenticatedService {
    public  UserInfo getUserInfo() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .filter(authentication -> authentication.getPrincipal() instanceof UserInfo)
                .map(authentication -> (UserInfo) authentication.getPrincipal())
                .orElse(null);
    }

    public  UUID getUserId() {
        return getUserInfo().getId();
    }
}
