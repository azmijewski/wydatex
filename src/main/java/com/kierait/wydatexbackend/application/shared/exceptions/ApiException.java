package com.kierait.wydatexbackend.application.shared.exceptions;

import java.util.UUID;

public class ApiException extends RuntimeException {
    UUID hashCode;

    public ApiException(String message) {
        super(message);
        hashCode = UUID.randomUUID();
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
        hashCode = UUID.randomUUID();
    }
}
