package com.kierait.wydatexbackend.application.messaging.sendingStrategy;

import com.kierait.wydatexbackend.application.messaging.Message;

public interface SendingStrategy {
    void sendMessage(Message message);
}
