package com.kierait.wydatexbackend.application.messaging.tracker;

import com.kierait.wydatexbackend.application.messaging.Message;
import com.kierait.wydatexbackend.application.messaging.MessageType;
import com.kierait.wydatexbackend.application.messaging.sendingStrategy.SendingStrategy;
import com.kierait.wydatexbackend.domain.user.UserRepository;
import com.kierait.wydatexbackend.domain.user.events.UserConfirmed;
import com.kierait.wydatexbackend.domain.user.events.UserCreated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.HashMap;

@Profile("!test")
@Component
@RequiredArgsConstructor
public class UserConfirmedListener {
    private static final String APP_URl = "appurl";
    private static final String LOGIN = "/login";
    private static final String DISPLAY_NAME = "display_name";

    private final UserRepository userRepository;
    private final SendingStrategy sendingStrategy;

    @Value("${app.guiUrl}")
    private String guiUrl;

    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Transactional
    public void on(UserConfirmed userConfirmed) {
        var user = userRepository.getOne(userConfirmed.getUserId());

        var payload = new HashMap<String, Object>();
        payload.put(DISPLAY_NAME, user.getDisplayName().getFirstname());
        payload.put(APP_URl, guiUrl + LOGIN);

        sendingStrategy.sendMessage(new Message(user.getEmail(), payload, MessageType.EMAIL_VERIFICATION));
    }

}
