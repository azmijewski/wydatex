package com.kierait.wydatexbackend.application.messaging;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@PropertySource(value = "classpath:subject.yml", encoding = "UTF-8")
@RequiredArgsConstructor
public class SubjectProvider {
    private final Environment environment;

    public String getSubjectForMessageType(MessageType messageType) {
        return Optional.ofNullable(environment.getProperty(messageType.name()))
                .orElseThrow(() -> new IllegalArgumentException(String.format("Could not find subject for message type %s", messageType)));
    }
}
