package com.kierait.wydatexbackend.application.messaging.sendingStrategy;

import com.kierait.wydatexbackend.application.messaging.Message;
import com.kierait.wydatexbackend.application.messaging.MessageType;
import com.kierait.wydatexbackend.application.messaging.SubjectProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
class JavaMailSendingStrategy implements SendingStrategy {
    private final JavaMailSender mailSender;
    private final ITemplateEngine templateEngine;
    private final SubjectProvider subjectProvider;

    @Override
    public void sendMessage(Message message) {
        var mimeMessage = mailSender.createMimeMessage();
        try {
            var helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(message.getTo());
            helper.setSubject(subjectProvider.getSubjectForMessageType(message.getMessageType()));
            helper.setText(buildTemplate(message.getPayload(), message.getMessageType()), true);
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            log.error("Could not send mail: {}", e.getMessage());
        }
    }

    private String buildTemplate(Map<String, Object> payload, MessageType messageType) {
        var context = new Context();
        context.setVariables(payload);
        return templateEngine.process(messageType.name(), context);
    }

}
