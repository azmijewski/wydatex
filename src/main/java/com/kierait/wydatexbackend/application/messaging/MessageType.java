package com.kierait.wydatexbackend.application.messaging;

public enum MessageType {
    REGISTRATION,
    EMAIL_VERIFICATION
}
