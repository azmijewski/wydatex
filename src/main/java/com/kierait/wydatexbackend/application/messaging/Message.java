package com.kierait.wydatexbackend.application.messaging;

import lombok.Value;

import java.io.Serializable;
import java.util.Map;

@Value
public class Message implements Serializable {
    String to;
    Map<String, Object> payload;
    MessageType messageType;
}
