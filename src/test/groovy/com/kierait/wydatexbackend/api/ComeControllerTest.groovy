package com.kierait.wydatexbackend.api

import com.fasterxml.jackson.core.type.TypeReference
import com.kierait.wydatexbackend.IntegrationSpecification
import com.kierait.wydatexbackend.application.come.model.ComeDTO
import com.kierait.wydatexbackend.domain.come.ComeRepository
import com.kierait.wydatexbackend.domain.come.ComeType
import com.kierait.wydatexbackend.domain.come.income.IncomeEntity
import com.kierait.wydatexbackend.domain.come.income.IncomeRepository
import com.kierait.wydatexbackend.domain.come.outcome.OutcomeRepository
import com.kierait.wydatexbackend.domain.user.UserEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.transaction.annotation.Transactional

import java.nio.charset.Charset
import java.time.LocalDate

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ComeControllerTest extends IntegrationSpecification {
    @Autowired
    ComeRepository comeRepository

    @Autowired
    IncomeRepository incomeRepository

    @Autowired
    OutcomeRepository outcomeRepository

    @Transactional
    def "should add income"() {
        given:
        def user = prepareEnabledUser()
        def comeDto = new ComeDTO(null, BigDecimal.TEN, LocalDate.now(), "test", null)
        def comeDtoJson = objectMapper.writeValueAsString(comeDto)
        def auth = "Basic " + HttpHeaders.encodeBasicAuth(user.email, "Asd12345", Charset.forName("UTF-8"))

        when:
        mockMvc.perform(post("/api/comes/incomes")
            .header("Authorization", auth)
            .contentType(MediaType.APPLICATION_JSON)
            .content(comeDtoJson))
        .andExpect(status().isCreated())

        then:
        def income = incomeRepository.findAllByUser(user)[0]
        income.date == comeDto.date
        income.price == comeDto.price
        income.description == comeDto.description
    }

    def "should get incomes beetwen dates"() {
        given:
        def now = LocalDate.now()
        def user = prepareEnabledUser()
        def income1 = prepareIncomes(BigDecimal.TEN, now, "test1", user)
        def income2 = prepareIncomes(BigDecimal.ONE, now.minusMonths(1), "test1", user)
        prepareIncomes(BigDecimal.valueOf(15), now.minusMonths(3), "test1", user)
        def auth = "Basic " + HttpHeaders.encodeBasicAuth(user.email, "Asd12345", Charset.forName("UTF-8"))

        when:
        def result = mockMvc.perform(get("/api/comes/incomes")
                .header("Authorization", auth)
                .param("startDate", now.minusMonths(2).toString())
                .param("endDate", now.toString()))
        .andReturn()

        then:
        def resultList = objectMapper.readValue(result.response.getContentAsString(), new TypeReference<List<ComeDTO>>(){})
        resultList.size() == 2
        resultList[0].id == income1.id
        resultList[0].description == income1.description
        resultList[0].price == income1.price
        resultList[0].date == income1.date
        resultList[0].comeType == ComeType.INCOME
        resultList[1].id == income2.id
        resultList[1].description == income2.description
        resultList[1].price == income2.price
        resultList[1].date == income2.date
        resultList[1].comeType == ComeType.INCOME
    }

    def prepareIncomes(BigDecimal price, LocalDate date, String description, UserEntity user) {
        def income = new IncomeEntity(price, date, description, user)
        return transactionTemplate.execute({t ->
            incomeRepository.save(income)
        })
    }
}
