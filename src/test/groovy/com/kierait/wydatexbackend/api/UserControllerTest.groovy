package com.kierait.wydatexbackend.api

import com.kierait.wydatexbackend.IntegrationSpecification
import com.kierait.wydatexbackend.application.user.model.RegisterUser
import com.kierait.wydatexbackend.application.user.model.UserDTO
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.transaction.annotation.Transactional

import java.nio.charset.Charset

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class UserControllerTest extends IntegrationSpecification {

    def "should register user"() {
        given:
        def password = "Asd12345".toCharArray()
        def register = new RegisterUser("test@test.pl", password, "Test", "Test")
        def registerJson = objectMapper.writeValueAsString(register)

        expect:
        mockMvc.perform(post("/api/users/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registerJson))
                .andExpect(status().isCreated())

        userRepository.existsByEmail("test@test.pl")
    }

    def "should not register if email exist"() {
        given:
        prepareUser("test@test.pl")
        def password = "Asd12345"
        def register = new RegisterUser("test@test.pl", password.toCharArray(), "Test", "Test")
        def registerJson = objectMapper.writeValueAsString(register)

        expect:
        mockMvc.perform(post("/api/users/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registerJson))
                .andExpect(status().isConflict())
    }

    def "should login"() {
        when:"if user exist and enabled"
        def enabledUser = prepareUser("enabled@test.pl", "Qwerty12345")
        enableUser(enabledUser)
        def enabledAuth = "Basic " + HttpHeaders.encodeBasicAuth("enabled@test.pl", "Qwerty12345", Charset.forName("UTF-8"))

        then:
        def result = mockMvc.perform(get("/api/users/login")
                .header("Authorization", enabledAuth))
            .andExpect(status().isOk())
            .andReturn()

        def stringContent = result.response.getContentAsString()
        def content = objectMapper.readValue(stringContent, UserDTO.class)
        content.id == enabledUser.id
        content.role == enabledUser.role
        content.email == enabledUser.email

        when:"if user exist but not enabled"
        prepareUser("notEnabled@test.pl", "Qwerty12345")
        def notEnabledAuth = "Basic " + HttpHeaders.encodeBasicAuth("notEnabled@test.pl", "Qwerty12345", Charset.forName("UTF-8"))

        then:
        mockMvc.perform(get("/api/users/login")
                .header("Authorization", notEnabledAuth))
                .andExpect(status().isUnauthorized())

        when:"if not exist"
        def wrongAuth = "Basic " + HttpHeaders.encodeBasicAuth("wrong@test.pl", "Qwerty12345", Charset.forName("UTF-8"))

        then:
        mockMvc.perform(get("/api/users/login")
                .header("Authorization", wrongAuth))
                .andExpect(status().isUnauthorized())
    }

    @Transactional
    def "should enable user"() {
        when: "if code exist"
        def user = prepareUser("test@test.pl")

        then:
        mockMvc.perform(put("/api/users/confirmation/{code}", user.confirmationCode))
            .andExpect(status().isOk())

        def resultUser = userRepository.getOne(user.id)
        resultUser.enabled
        resultUser.confirmationCode == null

        when:"if code not found"
        def userWrongCode = prepareUser("test1@test.pl")

        then:
        mockMvc.perform(put("/api/users/confirmation/{code}", UUID.randomUUID()))
                .andExpect(status().isNotFound())

        def resultUserNotConfirmed = userRepository.getOne(userWrongCode.id)
        !resultUserNotConfirmed.enabled
        resultUserNotConfirmed.confirmationCode != null
    }
}
