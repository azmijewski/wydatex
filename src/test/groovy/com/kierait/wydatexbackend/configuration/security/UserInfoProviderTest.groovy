package com.kierait.wydatexbackend.configuration.security

import com.kierait.wydatexbackend.domain.user.Role
import com.kierait.wydatexbackend.domain.user.UserEntity
import com.kierait.wydatexbackend.domain.user.UserRepository
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UsernameNotFoundException
import spock.lang.Specification
import spock.lang.Subject

class UserInfoProviderTest extends Specification{
    @Subject
    def provider

    def userRepository

    def setup() {
        userRepository = Mock(UserRepository)
        provider = new UserInfoProvider(userRepository)
    }

    def "should load user by username"() {
        given:
        def username = "test@test.pl"
        def user = new UserEntity(email: "test@test.pl", id: UUID.randomUUID(), role: Role.ROLE_USER, password: "TestTest123")

        when: "user exist and is enabled"
        user.enabled = true
        def result = provider.loadUserByUsername(username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(user)

        result.id == user.id
        result.email == username
        result.password == user.password
        result.grantedAuthorities == [new SimpleGrantedAuthority(Role.ROLE_USER.name())]

        when:"user exist but not enabled"
        user.enabled = false
        provider.loadUserByUsername(username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.of(user)
        def exception = thrown(UsernameNotFoundException)
        exception.message == "User not found"

        when:"user not exist"
        provider.loadUserByUsername(username)

        then:
        1 * userRepository.findByEmail(username) >> Optional.empty()
        exception = thrown(UsernameNotFoundException)
        exception.message == "User not found"
    }
}
