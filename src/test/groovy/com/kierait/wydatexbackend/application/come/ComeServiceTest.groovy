package com.kierait.wydatexbackend.application.come

import com.kierait.wydatexbackend.application.AuthenticatedService
import com.kierait.wydatexbackend.application.come.model.ComeDTO
import com.kierait.wydatexbackend.domain.come.ComeEntity
import com.kierait.wydatexbackend.domain.come.ComeRepository
import com.kierait.wydatexbackend.domain.come.ComeType
import com.kierait.wydatexbackend.domain.come.income.IncomeEntity
import com.kierait.wydatexbackend.domain.come.outcome.OutcomeEntity
import com.kierait.wydatexbackend.domain.user.UserEntity
import org.springframework.data.domain.Pageable
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate


class ComeServiceTest extends Specification{
    @Subject
    def service

    def comeRepository
    def comeMapper
    def authenticatedService

    def setup() {
        comeRepository = Mock(ComeRepository)
        comeMapper = Mock(ComeMapper)
        authenticatedService = Mock(AuthenticatedService)
        service = new ComeService(comeRepository, comeMapper, authenticatedService)
    }

    def "should delete come"() {
        given:
        def comeId = UUID.randomUUID()
        def userId = UUID.randomUUID()

        when:"come exist"
        service.deleteCome(comeId)

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * comeRepository.findByIdAndUserId(comeId, userId) >> Optional.of(new IncomeEntity())
        1 * comeRepository.delete(_ as ComeEntity)

        when:"come not exist"
        service.deleteCome(comeId)

        then:
        2 * authenticatedService.getUserId() >> userId
        1 * comeRepository.findByIdAndUserId(comeId, userId) >> Optional.empty()
        def exception = thrown(ComeNotFound)
        exception.message == String.format("Could not found come with id: %s for user: %s", comeId, userId)
    }

    def "should update come"() {
        given:
        def comeId = UUID.randomUUID()
        def userId = UUID.randomUUID()
        def now = LocalDate.now()
        def comeDTO = new ComeDTO(null, BigDecimal.TEN, now, "Test description", null)

        when:"come exist"
        service.updateCome(comeId, comeDTO)

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * comeRepository.findByIdAndUserId(comeId, userId) >> Optional.of(new IncomeEntity())
        1 * comeRepository.save(_ as ComeEntity) >> { ComeEntity it ->
            assert it.date == now
            assert it.description == "Test description"
            assert it.price == BigDecimal.TEN
        }

        when:"come not exist"
        service.updateCome(comeId, comeDTO)

        then:
        2 * authenticatedService.getUserId() >> userId
        1 * comeRepository.findByIdAndUserId(comeId, userId) >> Optional.empty()
        def exception = thrown(ComeNotFound)
        exception.message == String.format("Could not found come with id: %s for user: %s", comeId, userId)
    }

    def "should get last comes"() {
        given:
        def userId = UUID.randomUUID()
        def now = LocalDate.now()
        def income1 = prepareIncome(BigDecimal.valueOf(5), now)
        def outcome1 = prepareOutcome(BigDecimal.valueOf(12), now.minusDays(1))
        def income2 = prepareIncome(BigDecimal.valueOf(25), now.minusDays(2))
        def outcome2 = prepareOutcome(BigDecimal.valueOf(1), now.minusDays(3))
        def income3 = prepareIncome(BigDecimal.valueOf(14), now.minusDays(4))
        def outcome3 = prepareOutcome(BigDecimal.valueOf(8), now.minusDays(5))
        def income4 = prepareIncome(BigDecimal.TEN, now.minusDays(6))
        def comes = [income1, outcome1, income2, outcome2, income3, outcome3, income4]

        when:
        def result = service.getLastComes()

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * comeRepository.findByUserId(_ as UUID, _ as Pageable) >> comes
        1 * comeMapper.mapToDTO(income1) >> map(income1)
        1 * comeMapper.mapToDTO(income2) >> map(income2)
        1 * comeMapper.mapToDTO(income3) >> map(income3)
        1 * comeMapper.mapToDTO(income4) >> map(income4)
        1 * comeMapper.mapToDTO(outcome1) >> map(outcome1)
        1 * comeMapper.mapToDTO(outcome2) >> map(outcome2)
        1 * comeMapper.mapToDTO(outcome3) >> map(outcome3)

        result[0].price == income1.price
        result[0].date == income1.date
        result[1].price == outcome1.price
        result[1].date == outcome1.date
        result[2].price == income2.price
        result[2].date == income2.date
        result[3].price == outcome2.price
        result[3].date == outcome2.date
        result[4].price == income3.price
        result[4].date == income3.date
        result[5].price == outcome3.price
        result[5].date == outcome3.date
        result[6].price == income4.price
        result[6].date == income4.date
    }

    def "should get month comes"() {
        given:
        def userId = UUID.randomUUID()
        def now = LocalDate.now().withDayOfMonth(2)
        def income1 = prepareIncome(BigDecimal.valueOf(5), now)
        def outcome1 = prepareOutcome(BigDecimal.valueOf(12), now.minusDays(1))
        def income2 = prepareIncome(BigDecimal.valueOf(25), now.minusMonths(1))
        def outcome2 = prepareOutcome(BigDecimal.valueOf(1), now.minusMonths(3))
        def income3 = prepareIncome(BigDecimal.valueOf(14), now.minusMonths(4))
        def outcome3 = prepareOutcome(BigDecimal.valueOf(8), now.minusMonths(5))
        def income4 = prepareIncome(BigDecimal.TEN, now.minusMonths(6))
        def comes = [income1, outcome1, income2, outcome2, income3, outcome3, income4]

        when:
        def result = service.getMonthComes()

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * comeRepository.findAllByUserAfterDate(_ as UUID, _ as LocalDate) >> comes
        1 * comeMapper.mapToDTO(income1) >> map(income1)
        1 * comeMapper.mapToDTO(income2) >> map(income2)
        1 * comeMapper.mapToDTO(income3) >> map(income3)
        1 * comeMapper.mapToDTO(income4) >> map(income4)
        1 * comeMapper.mapToDTO(outcome1) >> map(outcome1)
        1 * comeMapper.mapToDTO(outcome2) >> map(outcome2)
        1 * comeMapper.mapToDTO(outcome3) >> map(outcome3)

        result.size() == 6
        result[0].month == now.getMonthValue()
        result[0].year == now.getYear()
        result[0].sum == BigDecimal.valueOf(-7)
        result[0].current
        result[0].comes.size() == 2
        result[1].month == now.minusMonths(1).getMonthValue()
        result[1].year == now.minusMonths(1).getYear()
        result[1].sum == BigDecimal.valueOf(25)
        !result[1].current
        result[1].comes.size() == 1
        result[2].month == now.minusMonths(3).getMonthValue()
        result[2].year == now.minusMonths(3).getYear()
        result[2].sum == BigDecimal.valueOf(-1)
        !result[2].current
        result[2].comes.size() == 1
        result[3].month == now.minusMonths(4).getMonthValue()
        result[3].year == now.minusMonths(4).getYear()
        result[3].sum == BigDecimal.valueOf(14)
        !result[3].current
        result[3].comes.size() == 1
        result[4].month == now.minusMonths(5).getMonthValue()
        result[4].year == now.minusMonths(5).getYear()
        result[4].sum == BigDecimal.valueOf(-8)
        !result[4].current
        result[4].comes.size() == 1
        result[5].month == now.minusMonths(6).getMonthValue()
        result[5].year == now.minusMonths(6).getYear()
        result[5].sum == BigDecimal.valueOf(10)
        !result[5].current
        result[5].comes.size() == 1
    }

    def prepareIncome(price = BigDecimal.TEN, date = LocalDate.now(), desc = "Test description", userId = UUID.randomUUID()) {
        return new IncomeEntity(price, date, desc, new UserEntity(id: userId))
    }

    def prepareOutcome(price = BigDecimal.TEN, date = LocalDate.now(), desc = "Test description", userId = UUID.randomUUID()) {
        return new OutcomeEntity(price, date, desc, new UserEntity(id: userId))
    }

    def map(ComeEntity comeEntity) {
        return new ComeDTO(comeEntity.id, comeEntity.price, comeEntity.date, comeEntity.description, comeEntity instanceof IncomeEntity ? ComeType.INCOME : ComeType.OUTCOME)
    }
}
