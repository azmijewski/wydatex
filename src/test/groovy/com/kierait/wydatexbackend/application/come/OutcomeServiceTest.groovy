package com.kierait.wydatexbackend.application.come

import com.kierait.wydatexbackend.application.AuthenticatedService
import com.kierait.wydatexbackend.application.come.model.ComeDTO
import com.kierait.wydatexbackend.domain.come.ComeType
import com.kierait.wydatexbackend.domain.come.outcome.OutcomeEntity
import com.kierait.wydatexbackend.domain.come.outcome.OutcomeRepository
import com.kierait.wydatexbackend.domain.user.UserEntity
import com.kierait.wydatexbackend.domain.user.UserRepository
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate

class OutcomeServiceTest extends Specification{
    @Subject
    def service

    def outcomeRepository
    def userRepository
    def comeMapper
    def authenticatedService

    def setup(){
        outcomeRepository = Mock(OutcomeRepository)
        userRepository = Mock(UserRepository)
        comeMapper = Mock(ComeMapper)
        authenticatedService = Mock(AuthenticatedService)
        service = new OutcomeService(outcomeRepository, userRepository,
                comeMapper, authenticatedService)
    }

    def "should add outcome"() {
        given:
        def userId = UUID.randomUUID()
        def user = new UserEntity(id: userId)
        def comeDTO = new ComeDTO(UUID.randomUUID(), BigDecimal.TEN, LocalDate.now(), "Description test", ComeType.OUTCOME);

        when:
        service.addOutcome(comeDTO)

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * userRepository.getOne(userId) >> user
        1 * outcomeRepository.save(_ as OutcomeEntity) >> { OutcomeEntity it ->
            assert it.date == comeDTO.date
            assert it.description == comeDTO.description
            assert it.price == comeDTO.price
            assert it.user.id == userId
        }
    }

    def "should get all by user between date"() {
        given:
        def userId = UUID.randomUUID()
        def outcomeEntity1 = new OutcomeEntity(BigDecimal.TEN, LocalDate.now(), "Desc1", null)
        def outcomeEntity2 = new OutcomeEntity(BigDecimal.TEN, LocalDate.now(), "Desc2", null)

        when:
        def result = service.getAllByUserBetweenDate(LocalDate.now().minusDays(5), LocalDate.now())

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * outcomeRepository.findAllByUserIdAndDateBetween(_, _, _) >> [outcomeEntity1, outcomeEntity2]
        1 * comeMapper.mapToDTO(outcomeEntity1) >> new ComeDTO(outcomeEntity1.id, outcomeEntity1.price, outcomeEntity1.date, outcomeEntity1.description, ComeType.OUTCOME)
        1 * comeMapper.mapToDTO(outcomeEntity2) >> new ComeDTO(outcomeEntity2.id, outcomeEntity2.price, outcomeEntity2.date, outcomeEntity2.description, ComeType.OUTCOME)

        result[0].id == outcomeEntity1.id
        result[0].price == outcomeEntity1.price
        result[0].date == outcomeEntity1.date
        result[0].description == outcomeEntity1.description
        result[0].comeType == ComeType.OUTCOME
        result[1].id == outcomeEntity2.id
        result[1].price == outcomeEntity2.price
        result[1].date == outcomeEntity2.date
        result[1].description == outcomeEntity2.description
        result[1].comeType == ComeType.OUTCOME
    }
}
