package com.kierait.wydatexbackend.application.come

import com.kierait.wydatexbackend.application.AuthenticatedService
import com.kierait.wydatexbackend.application.come.model.ComeDTO
import com.kierait.wydatexbackend.domain.come.ComeType
import com.kierait.wydatexbackend.domain.come.income.IncomeEntity
import com.kierait.wydatexbackend.domain.come.income.IncomeRepository
import com.kierait.wydatexbackend.domain.user.UserEntity
import com.kierait.wydatexbackend.domain.user.UserRepository
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate

class IncomeServiceTest extends Specification{
    @Subject
    def service

    def incomeRepository
    def userRepository
    def comeMapper
    def authenticatedService

    def setup(){
        incomeRepository = Mock(IncomeRepository)
        userRepository = Mock(UserRepository)
        comeMapper = Mock(ComeMapper)
        authenticatedService = Mock(AuthenticatedService)
        service = new IncomeService(incomeRepository, userRepository,
                comeMapper, authenticatedService)
    }

    def "should add income"() {
        given:
        def userId = UUID.randomUUID()
        def user = new UserEntity(id: userId)
        def comeDTO = new ComeDTO(UUID.randomUUID(), BigDecimal.TEN, LocalDate.now(), "Description test", ComeType.INCOME);

        when:
        service.addIncome(comeDTO)

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * userRepository.getOne(userId) >> user
        1 * incomeRepository.save(_ as IncomeEntity) >> { IncomeEntity it ->
            assert it.date == comeDTO.date
            assert it.description == comeDTO.description
            assert it.price == comeDTO.price
            assert it.user.id == userId
        }
    }

    def "should get all by user between date"() {
        given:
        def userId = UUID.randomUUID()
        def incomeEntity1 = new IncomeEntity(BigDecimal.TEN, LocalDate.now(), "Desc1", null)
        def incomeEntity2 = new IncomeEntity(BigDecimal.TEN, LocalDate.now(), "Desc2", null)

        when:
        def result = service.getAllByUserBetweenDate(LocalDate.now().minusDays(5), LocalDate.now())

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * incomeRepository.findAllByUserIdAndDateBetween(_, _, _) >> [incomeEntity1, incomeEntity2]
        1 * comeMapper.mapToDTO(incomeEntity1) >> new ComeDTO(incomeEntity1.id, incomeEntity1.price, incomeEntity1.date, incomeEntity1.description, ComeType.INCOME)
        1 * comeMapper.mapToDTO(incomeEntity2) >> new ComeDTO(incomeEntity2.id, incomeEntity2.price, incomeEntity2.date, incomeEntity2.description, ComeType.INCOME)

        result[0].id == incomeEntity1.id
        result[0].price == incomeEntity1.price
        result[0].date == incomeEntity1.date
        result[0].description == incomeEntity1.description
        result[0].comeType == ComeType.INCOME
        result[1].id == incomeEntity2.id
        result[1].price == incomeEntity2.price
        result[1].date == incomeEntity2.date
        result[1].description == incomeEntity2.description
        result[1].comeType == ComeType.INCOME
    }
}
