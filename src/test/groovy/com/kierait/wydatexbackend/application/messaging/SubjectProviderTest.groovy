package com.kierait.wydatexbackend.application.messaging

import org.springframework.core.env.Environment
import spock.lang.Specification
import spock.lang.Subject

class SubjectProviderTest extends Specification{
    @Subject
    def provider

    def env

    def setup() {
        env = Mock(Environment)
        provider = new SubjectProvider(env)
    }

    def "should provide subject"() {
        when:
        def result = provider.getSubjectForMessageType(MessageType.EMAIL_VERIFICATION)

        then:
        1 * env.getProperty(MessageType.EMAIL_VERIFICATION.name()) >> "test"
        result == "test"
    }

    def "should throw exception if invalid message type"() {
        when:
        provider.getSubjectForMessageType(MessageType.REGISTRATION)

        then:
        1 * env.getProperty(MessageType.REGISTRATION.name()) >> null
        def exc = thrown(IllegalArgumentException)
        exc.message == "Could not find subject for message type " + MessageType.REGISTRATION
    }
}
