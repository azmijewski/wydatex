package com.kierait.wydatexbackend.application.messaging.tracker

import com.kierait.wydatexbackend.application.messaging.Message
import com.kierait.wydatexbackend.application.messaging.MessageType
import com.kierait.wydatexbackend.application.messaging.sendingStrategy.SendingStrategy
import com.kierait.wydatexbackend.domain.user.DisplayName
import com.kierait.wydatexbackend.domain.user.UserEntity
import com.kierait.wydatexbackend.domain.user.UserRepository
import com.kierait.wydatexbackend.domain.user.events.UserCreated
import spock.lang.Specification
import spock.lang.Subject

class UserCreatedListenerTest extends Specification{
    @Subject
    def listener

    def userRepository
    def sendingStrategy

    def setup() {
        userRepository = Mock(UserRepository)
        sendingStrategy = Mock(SendingStrategy)
        listener = new UserCreatedListener(userRepository, sendingStrategy)
        listener.guiUrl = "http://localhost:4200"
    }

    def "should listen on event"() {
        given:
        def user = new UserEntity(displayName: new DisplayName("Test", "Test"), confirmationCode: UUID.randomUUID())
        def event = new UserCreated(UUID.randomUUID())

        when:
        listener.on(event)

        then:
        1 * userRepository.getOne(event.userId) >> user
        1 * sendingStrategy.sendMessage(_ as Message) >> { Message it ->
            assert it.messageType == MessageType.REGISTRATION
            assert it.payload[listener.DISPLAY_NAME] == user.displayName.firstname
            assert it.payload[listener.APP_URl] == listener.guiUrl + String.format(listener.EMAIL_CONFIRM, user.getConfirmationCode())
        }
    }
}
