package com.kierait.wydatexbackend.application.messaging.sendingStrategy

import com.kierait.wydatexbackend.application.messaging.Message
import com.kierait.wydatexbackend.application.messaging.MessageType
import com.kierait.wydatexbackend.application.messaging.SubjectProvider
import org.springframework.mail.javamail.JavaMailSender
import org.thymeleaf.ITemplateEngine
import org.thymeleaf.context.Context
import spock.lang.Specification
import spock.lang.Subject

import javax.mail.internet.MimeMessage

class JavaMailSendingStrategyTest extends Specification{
    @Subject
    def sendingStrategy

    def mailSender
    def templateEngine
    def subjectProvider

    def setup() {
        mailSender = Mock(JavaMailSender)
        templateEngine = Mock(ITemplateEngine)
        subjectProvider = Mock(SubjectProvider)
        sendingStrategy = new JavaMailSendingStrategy(mailSender, templateEngine, subjectProvider)
    }

    def "should send message"() {
        given:
        def message = new Message("test@test.pl", new HashMap<String, Object>(), MessageType.REGISTRATION)
        def mimeMessage = new MimeMessage(null)

        when:
        sendingStrategy.sendMessage(message)

        then:
        1 * mailSender.createMimeMessage() >> mimeMessage
        1 * subjectProvider.getSubjectForMessageType(MessageType.REGISTRATION) >> "test"
        1 * templateEngine.process(_ as String, _ as Context) >> "test"
        1 * mailSender.send(_ as MimeMessage)
    }

}
