package com.kierait.wydatexbackend.application.user

import com.kierait.wydatexbackend.application.AuthenticatedService
import com.kierait.wydatexbackend.application.user.model.RegisterUser
import com.kierait.wydatexbackend.application.user.model.UserDTO
import com.kierait.wydatexbackend.domain.user.DisplayName
import com.kierait.wydatexbackend.domain.user.Role
import com.kierait.wydatexbackend.domain.user.UserEntity
import com.kierait.wydatexbackend.domain.user.UserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class UserServiceTest extends Specification {
    @Subject
    def userService

    def userFactory
    def userRepository
    def authenticatedService
    def userMapper

    def setup() {
        userFactory = new UserFactory(Mock(PasswordEncoder))
        userRepository = Mock(UserRepository)
        authenticatedService = Mock(AuthenticatedService)
        userMapper = Mock(UserMapper)
        userService = new UserService(userRepository, userFactory, userMapper, authenticatedService)
    }

    def "should register user"() {
        given:
        def register = new RegisterUser("test@test.pl", "asd12345".toCharArray(), "Test", "Test")

        when:"when email not exist"
        userService.registerUser(register)

        then:
        1 * userRepository.existsByEmail("test@test.pl") >> false
        1 * userRepository.save(_ as UserEntity) >> { UserEntity it ->
            assert it.displayName.firstname == "Test"
            assert it.displayName.lastname == "Test"
            assert it.role == Role.ROLE_USER
            assert !it.enabled
        }
        register.password == "********".toCharArray()

        when:"when email exist"
        userService.registerUser(register)

        then:
        1 * userRepository.existsByEmail("test@test.pl") >> true

        def exception = thrown(UserAlreadyExist)
        exception.message == "User: test@test.pl already exist"
        register.password == "********".toCharArray()
    }

    def "should get logged user"() {
        given:
        def userId = UUID.randomUUID()
        def user = new UserEntity("test@test.pl", "Asd12345", new DisplayName("Test", "Test"))

        when:
        def result = userService.getLoggedUser()

        then:
        1 * authenticatedService.getUserId() >> userId
        1 * userRepository.getOne(userId) >> user
        1 * userMapper.mapToDTO(user) >> new UserDTO(userId, "test@test.pl", new DisplayName("Test", "Test"), Role.ROLE_USER)

        result.id == userId
        result.displayName.firstname == "Test"
        result.displayName.lastname == "Test"
        result.email == "test@test.pl"
        result.role == Role.ROLE_USER
    }
}
