package com.kierait.wydatexbackend

import com.kierait.wydatexbackend.domain.user.DisplayName
import com.kierait.wydatexbackend.domain.user.UserEntity
import com.kierait.wydatexbackend.domain.user.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.support.TransactionTemplate
import org.testcontainers.containers.PostgreSQLContainer
import com.fasterxml.jackson.databind.ObjectMapper
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/clear-db.sql")
@Testcontainers
class IntegrationSpecification extends Specification {

    @Shared
    static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer()
            .withDatabaseName("test")

    static {
        postgreSQLContainer.start()
        System.setProperty("db.testcontainer.username", postgreSQLContainer.username)
        System.setProperty("db.testcontainer.password", postgreSQLContainer.password)
        System.setProperty("db.testcontainer.url", postgreSQLContainer.jdbcUrl)
    }

    @Autowired
    TransactionTemplate transactionTemplate

    @Autowired
    MockMvc mockMvc

    @Autowired
    UserRepository userRepository

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    PasswordEncoder passwordEncoder

    def prepareUser(String email = "test@test.com", String password = "Asd12345") {
        def user = new UserEntity(email, passwordEncoder.encode(password), new DisplayName("Jan", "Kowalski"))
        return transactionTemplate.execute({t ->
            userRepository.save(user)
        })
    }

    def enableUser(UserEntity user) {
        user.enable()
        transactionTemplate.execute({t ->
            userRepository.save(user)
        })
    }

    def prepareEnabledUser() {
        def user = prepareUser()
        enableUser(user)
        return user
    }
}
